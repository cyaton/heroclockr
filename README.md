# HeroClockr

## Description

HeroClockr is a website that tracks personal goals and tries to incentivize these by decorating creatable streak attempts with medals and ranks according to their ongoing duration.

The app is nothing special and meant as a showcase for a project

- Written entirely in vanilla Javascript.
- Creating data persistence with full CRUD through local memory.
- Utilizing modularisation through imports.
- Practicing code commenting, commit documentation and transparent naming.
- Splitting selectors separate.
- Prototyping time related functions in an object.

## Installation

HeroClockr can be run in the browser and stores streak names and dates needed to provide its service locally in your browser. No data will be sent to the server. Visit a working example on [heroclockr.netlify.app](https://heroclockr.netlify.app/).

## Usage

New streaks can be created when tapping the 'Attempt Streak' button. Other options like `reset` show when tapping the streak itself. Please be aware that streaks will be lost if browser storage is deleted which can happen if the browser history is deleted.

### Privacy

Please note that streak names should not contain sensitive information because they are stored in browser memory and are theoretically accessible by all apps and websites using the browser local storage.

## Support

Feel free to contact me here on Gitlab.

## Feedback

Any feedback, suggestions or proper collaboration attempts are very welcome! Especially for other projects!

## Roadmap

Project finished → → → A new version of the project will be started:

- Named Heroclock2 on Cyatons Gitlab account
- Uses React and Material UI as front end
- Persistent storage through API to database

## Authors and acknowledgment

Sole contributor: Philipp Montazem

### Acknowledgments

#### Mozilla

Thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

#### Andy Lorenz

Handling arrays in local storage was very much alleviated by incorporating Andy Lorenz's post on [Stack Overflow](https://stackoverflow.com/a/23516713). Thanks!

#### Chris Coyler

Thanks to Chris Coylers useful articles on [css-tricks](https://css-tricks.com/) which I encounter frequently on my web searches for front end related stuff.

## LICENSE

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Project finished August, 4th 2023, but still receives occasional updates
