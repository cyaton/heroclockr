export const body = document.querySelector('body');
// System Message text paragraph
export const sysMessageP = document.querySelector('#sysMessage');
// A row with info only present if no streaks exists
export const noStreaksRow = document.querySelector('#noStreaksRow');
// The area where the input forms will be displayed
export const streakFormRow = document.querySelector('#streakFormRow');
// Table with the streaks list
export const streakList = document.querySelector('#streakList');
// Button row inside streak panel inside streak
export const streakControls = document.querySelector('#streakControls');
// Input form for streak name inside streakFormRow
export const inputStreakName = document.querySelector('#inputStreakName');
// The area containing the button to create new streak
export const rowBtn = document.querySelector('#rowBtn');
// The 'Attempt Streak' button
export const attemptStreakBtn = document.querySelector('#attemptStreak');
// The main button to create a new streak
export const streakBtn = document.querySelector('#rowBtn');

// Functions creating html elements
// Arrow functions because the function will then create a new separate element

export const createHtmlElement = (tagName = 'div', className = '', id = '', innerText = '', type = '') => {
    let element = document.createElement(`${tagName}`);
    className === '' ? null : element.className = className;
    id === '' ? null : element.id = id;
    innerText === '' ? null : element.innerText = innerText;
    type === '' ? null : element.setAttribute('type', type);
    return element;
}
export const createInput = (type, name, placeholder, width, value, required) => {
    let input = document.createElement('input');
    input.setAttribute('type', type);
    input.setAttribute('name', name);
    input.setAttribute('placeholder', placeholder);
    input.setAttribute('width', width);
    value === '' ? null : input.setAttribute('value', value);
    required ? input.setAttribute('required', '') : null;
    return input;
}

