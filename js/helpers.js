import { sysMessageP } from "./selectors.js";

export const pathMedalImg = '/img/medal/';
export const pathMedalVid = '/vid/medal/';

// Entity map for sanitizing strings.
    export const entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };

// Uses the Entity map for sanitizing strings
export function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}

// converts string to camelCase
export function camelCase(str) {
    str = str.toLowerCase()
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
        return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
}

// Display system message
export function sysMessage(text) {
    let message = sysMessageP

    message.innerText = text;
    setTimeout(() => {
        message.innerText = ''
    }, 3000)
}

// Selects medal according to streak duration
export function calcMedal(numberOfDays) {
    if (numberOfDays < 3) {
        return 't1StarIron';
    } else if (numberOfDays < 5) {
        return 't2StarBronze';
    } else if (numberOfDays < 7) {
        return 't3StarSilver';
    } else {
        return 't4StarGold';
    }
}

// Returns rank according to streak duration
export function calcRank(days) {
    return days < 3
        ? 'Follower'
        : days < 5
            ? 'Member'
            : days < 7
                ? 'Practicant'
                : days < 10
                    ? 'Practitioner'
                    : days < 15
                        ? 'Leader'
                        : days < 21
                            ? 'Master'
                            : 'Grandmaster';
}

// Checks if the browser supports HEVC codec with alpha by testing for api only existing in newer browsers of safari
export function supportsHEVCAlpha() {
    const navigator = window.navigator;
    const ua = navigator.userAgent.toLowerCase()
    const hasMediaCapabilities = !!(navigator.mediaCapabilities && navigator.mediaCapabilities.decodingInfo)
    const isSafari = ((ua.indexOf('safari') != -1) && (!(ua.indexOf('chrome') != -1) && (ua.indexOf('version/') != -1)))
    return isSafari && hasMediaCapabilities
}
