
// Set string value from data
export function setItem(key, item) {
    localStorage.setItem(key, `${item}`);
}

// retrieve Data
export function getItem(key) {
    return localStorage.getItem(key);
}

// Delete one entry
export function localstorageRemove(key) {
    return localStorage.removeItem(key);
}