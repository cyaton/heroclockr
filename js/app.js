import {getItem, localstorageRemove, setItem} from './storageSingleKey.js';
import Storage from './storageArrays.js';
import Time from './timeHelpers.js';
import {
    attemptStreakBtn,
    noStreaksRow,
    rowBtn,
    streakBtn,
    streakFormRow,
    streakList,
    createHtmlElement,
} from "./selectors.js";
import {
    calcMedal,
    calcRank,
    supportsHEVCAlpha,
    sysMessage,
    pathMedalImg,
    pathMedalVid
} from './helpers.js';
import btnStandard from "./components/btnStandard.js";
import formStreakName from "./components/formStreakName.js";

// When Attempt Streak button is pressed to create a new streak
function attemptStreak() {
    let streaks = localStorage.getArray('streaks');
    if (streaks.length <= 20) {
        // Show input form for new Streak by removing hide class
        streakList.insertAdjacentElement('afterend', formStreakName('My streak name'))

        // Hide streak attempt button
        rowBtn.style.display = 'none';

        // Removes Info when no Streaks are set.
        if (streaks.length === 0) { noStreaksRow.remove() }

        // input submission via button triggers createStreak()
    } else {
        return sysMessage('Sorry, too many streaks tracked.');
        streakFormRow.remove();
    }
}

function openUpdateStreakForm (id) {
    let streakControls = document.querySelector(`#${id}`).firstElementChild.firstElementChild.lastElementChild.lastElementChild;
    streakControls.style.display = 'none';
    rowBtn.style.display = 'none';
    const formValue = getItem(id + 'Name');
    streakControls.insertAdjacentElement('beforebegin', formStreakName('', formValue, id, true));
}

// Triggered from reset button in the edit panel
function resetStreak(id) {
    setItem(id, Date.now());
    location.reload();
}

// Triggered from delete button inside edit panel
function deleteStreak(id) {
    // Remove Streak from streaks list
    localStorage.deleteArrayItem('streaks', id);
    // Remove streak timestamp from local memory
    localstorageRemove(id);
    // Remove streak name from local memory
    localstorageRemove(id + 'Name');

    location.reload();
}

// Triggered when <tr> of streak row is pressed
function triggerStreakPanel(rowId) {
    try {
        // Row panel exists
        let rowPanel = document.querySelector(`#${rowId}`).children[0].children[0].children[1]
        if (rowPanel.className === 'panel' && rowPanel.style.display === 'none') {
            window.setTimeout(function () {
                rowPanel.style.display = null;
                rowPanel.className = 'panel'
            }, 1);
        } else {
            window.setTimeout(function () {
                rowPanel.style.display = 'none';
            }, 1); // time this to fit the animation
        }
    } catch (error) {
        // row panel does not exist yet - domStreakPanel creates it
        domStreakPanel(rowId);
    }
}

// ====== Listeners

// Notices a streak attempt when button #attemptStreak is pressed
attemptStreakBtn.addEventListener('click', () => { attemptStreak() })

// Listens to pressed streak row
streakList.addEventListener('click', (e) => {
    if (e.target.className === 'shadBoxOuter') {
        triggerStreakPanel(e.target.parentElement.id);
    }
    if (e.target.className === 'shadBoxInner') {
        triggerStreakPanel(e.target.parentElement.parentElement.id);
    }
    if (e.target.id === 'streakRow1') {
        triggerStreakPanel(e.target.parentElement.parentElement.parentElement.id);
    }
    if (e.target.id === 'btnUpdate') {
        openUpdateStreakForm(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id);
    }
    if (e.target.id === 'btnReset') {
        resetStreak(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id);
    }
    if (e.target.id === 'btnDelete') {
        deleteStreak(e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id);
    }
})

// ====== DOM component builders

// One row representing a streak
// elementAfter is the first element in the streak table after the streak rows - a button
export function domStreak(id, elementAfter) {
    // Get Name
    let name = getItem(id + 'Name');

    // Set tr
    let tr = createHtmlElement('tr', 'streakRow', id);
    elementAfter.insertAdjacentElement('beforebegin', tr);
    // Set tr > td
    let td = createHtmlElement('td', 'shadBoxOuter')
    tr.append(td);
    // Set tr > td > div
    let div = createHtmlElement(undefined, 'shadBoxInner');
    td.append(div);
    // Set tr > td > div > div
    let divRow1 = createHtmlElement(undefined, 'flx flxAlignBase', 'streakRow1');
    div.append(divRow1);
    // Set Streak title
    let h3 = createHtmlElement('h3', 'streakTitle noClick', undefined, name);
    divRow1.append(h3);

    // Define time parameters
    const startTime = new Time(parseInt(getItem(id)));
    const duration = startTime.duration();
    const days = startTime.days(startTime);

    // Set start time
    let p = createHtmlElement('p', 'noClick', undefined, `Started ${startTime.printTime(startTime)}`);
    h3.insertAdjacentElement('afterend', p);
    // Set duration
    let pDur = createHtmlElement('p', 'streakDuration noClick', undefined, duration);
    p.insertAdjacentElement('afterend', pDur);
    // Set medal
    let medal = createHtmlElement('img', 'iconInRow noClick');
    medal.alt = `${calcMedal(days)} achieved`;
    medal.src = pathMedalImg + calcMedal(days )+ 'Icon.60x60.png';
    pDur.insertAdjacentElement('afterend', medal);
}

// Info panel inside streak row
function domStreakPanel(rowId) {
    // Abort if not a streak row
    if (rowId === 'rowBtn' || rowId === 'noStreaksRow' || rowId === '') {
        return;
    }

    // Streak name
    let streakRow = document.querySelector(`#${rowId}`)
    // Set panel container
    const container = createHtmlElement('div', 'panel');
    streakRow.children[0].children[0].append(container);

    // Define Time parameters
    const startTime = new Time(parseInt(getItem(rowId)));
    const days = startTime.days(startTime);

    // Display medal
    let medalAni = createHtmlElement('video', 'aniM');
    let medal = calcMedal(days);
    medalAni.poster = pathMedalImg + medal + 'Poster.100x100.png';
    medalAni.autoplay = true;
    medalAni.muted = true;
    medalAni.playsInline = true
    medalAni.controls = false;
    medalAni.loop = true;
    medalAni.src = pathMedalVid + medal + (supportsHEVCAlpha() ? 'HEVC.400x400.mov' : 'VP8.400x400.webm');

    container.append(medalAni);

    // Display rank
    let rankElement = createHtmlElement('p', 'titleS mb0 mt05', undefined, calcRank(days) + ' Rank');
    medalAni.insertAdjacentElement('afterend', rankElement);

    // Controls
    let boxControls = createHtmlElement('div', 'flx mb1 mt2 streakControls');
    rankElement.insertAdjacentElement('afterend', boxControls);

    // Edit Btn
    let btnUpdate = btnStandard('btnUpdate', 'Edit');
    boxControls.append(btnUpdate);


    // Reset Btn
    let btnReset = btnStandard('btnReset', 'Reset');
    boxControls.append(btnReset);

    // Delete Btn
    let btnDelete = btnStandard('btnDelete', 'Delete');
    boxControls.append(btnDelete);
}

// ====== Init

// Check if local storage exists
let streaks = getItem('streaks');
// Removes Info 'no streaks are set' when streaks are found
if (streaks.length > 0) {
    noStreaksRow.remove();
    // Loops through streaks array to create the streak entries
    for (const streakName of localStorage.getArray('streaks')) {
        domStreak(streakName, streakBtn);
    }
}
