// ====== Storage handler
// Thanks to Andy Lorenz's answer posted on https://stackoverflow.com/a/23516713

// .getArray(arrayName); -> returns entire array
Storage.prototype.getArray = function (arrayName) {
    let thisArray = [];
    let fetchArrayObject = this.getItem(arrayName);
    if (typeof fetchArrayObject !== 'undefined') {
        if (fetchArrayObject !== null) {
            thisArray = JSON.parse(fetchArrayObject);
        }
    }
    return thisArray;
}

// .pushArrayItem(arrayName,arrayItem); -> adds an element onto end of named array
Storage.prototype.pushArrayItem = function (arrayName, arrayItem) {
    let existingArray = this.getArray(arrayName);
    existingArray.push(arrayItem);
    this.setItem(arrayName, JSON.stringify(existingArray));
}

// .popArrayItem(arrayName); -> removes & returns last array element
Storage.prototype.popArrayItem = function (arrayName) {
    let arrayItem = {};
    let existingArray = this.getArray(arrayName);
    if (existingArray.length > 0) {
        arrayItem = existingArray.pop();
        this.setItem(arrayName, JSON.stringify(existingArray));
    }
    return arrayItem;
}

// .shiftArrayItem(arrayName); -> removes & returns first array element
Storage.prototype.shiftArrayItem = function (arrayName) {
    let arrayItem = {};
    let existingArray = this.getArray(arrayName);
    if (existingArray.length > 0) {
        arrayItem = existingArray.shift();
        this.setItem(arrayName, JSON.stringify(existingArray));
    }
    return arrayItem;
}

// .unshiftArrayItem(arrayName,arrayItem); -> adds an element onto front of named array
Storage.prototype.unshiftArrayItem = function (arrayName, arrayItem) {
    let existingArray = this.getArray(arrayName);
    existingArray.unshift(arrayItem);
    this.setItem(arrayName, JSON.stringify(existingArray));
}

// .deleteArray(arrayName); -> removes entire array from storage
Storage.prototype.deleteArray = function (arrayName) {
    this.removeItem(arrayName);
}

// .deleteArrayItem(arrayName, arrayItem); ➟ removes one element by value from array and returns new array
Storage.prototype.deleteArrayItem = function (arrayName, arrayItem) {
    const existingArray = this.getArray(arrayName);
    console.log(existingArray);
    const modifiedArray = existingArray.filter(string => string !== arrayItem);
    console.log(modifiedArray);
    this.setItem(arrayName, JSON.stringify(modifiedArray));
}

export default Storage
