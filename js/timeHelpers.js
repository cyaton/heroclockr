function Time(date) {
    this.date = new Date(date)
}

Time.prototype.getMonth = function () {
    let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ]
    return months[this.date.getMonth()]
}
Time.prototype.getMinutes = function () {
    let minutes = this.date.getMinutes();
    minutes < 10 ? minutes = '0' + minutes : null;
    return minutes
}
Time.prototype.days = function () {
    return Math.floor((Date.now() - this.date) / (1000 * 3600 * 24));
}
Time.prototype.duration = function () {
    let duration = (Date.now() - this.date) / (1000 * 3600);
    if (duration < 24) {
        return duration.toFixed(0) + ' hrs';
    } else {
        return (duration / 24).toFixed(0) + ' days';
    }
}

// Converts timestamp from start time to readable format
Time.prototype.printTime = function () {
    return `${this.date.getDate()}. ${this.getMonth()} ${this.date.getFullYear()}, ${this.date.getHours()}:${this.getMinutes()}`;
}

export default Time