import { createHtmlElement } from "../selectors.js";

function btnStandard (btnID, btnText, type = 'button', containerID = '') {
    let boxBtn = createHtmlElement(undefined, 'shadBoxOuter shadBoxBtn', containerID);

    let btn = createHtmlElement('button', 'shadBoxInner colBlack fontSize1 pa05', btnID, btnText, type);
    boxBtn.append(btn);

    return boxBtn
}

export default btnStandard