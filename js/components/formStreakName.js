import {
    createHtmlElement,
    createInput,
    inputStreakName, rowBtn,
    streakFormRow
} from "../selectors.js";
import {domStreak} from "../app.js";
import btnStandard from "./btnStandard.js";
import {camelCase, escapeHtml, sysMessage} from "../helpers.js";
import {setItem} from "../storageSingleKey.js";

function formStreakName(placeholder, value = '', streakID = '', streakUpdate = false) {

    // Containers
    let boxStreakFormRow = createHtmlElement(undefined, 'm05', 'streakFormRow');

    let boxShadBoxOuter = createHtmlElement(undefined, 'shadBoxOuter');
    boxStreakFormRow.append(boxShadBoxOuter);

    let boxShadBoxInner = createHtmlElement(undefined, 'shadBoxInner flx flxAlignCenter');
    boxShadBoxOuter.append(boxShadBoxInner);

    // Form
    let divForm = createHtmlElement(undefined, 'flxGrow');
    boxShadBoxInner.append(divForm);

    let formLabel = createHtmlElement('label',  'mr1', undefined, 'Name');
    formLabel.setAttribute('for', 'inputStreakName');
    divForm.append(formLabel);

    let formInput = createInput('text', 'streakTitleInput', placeholder, 'inherit', value, true);
    formInput.id = 'inputStreakName';
    divForm.append(formInput);

    function undoFormDOM() {
        boxStreakFormRow.remove();
        // Show streak attempt button again
        streakID === '' ? null : document.querySelector(`#${streakID}`).firstElementChild.firstElementChild.lastElementChild.lastElementChild.style.display = '';
        rowBtn.style.display = '';
    }

    // Triggered when streak attempt data was sent
    function createStreak(streakName) {
        // Get streak name from input
        streakName = escapeHtml(formInput.value);
        let nameCCase = camelCase(streakName);
        // Hide Input form
        boxStreakFormRow.remove();

        // ==== Store data
        // Store streaks array in local storage
        localStorage.pushArrayItem('streaks', nameCCase);
        // Save date
        setItem(nameCCase, Date.now());
        // Save name
        setItem(nameCCase + 'Name', streakName);
        // Build structure for new streak row
        domStreak(nameCCase, rowBtn);

        // Reset globals.
        formInput.value = null;

        // Show streak attempt button again.
        rowBtn.style.display = '';
    }
    // Submit button
    const btnSubmit = btnStandard('updateStreakBtn', 'Set', 'submit');
    boxShadBoxInner.append(btnSubmit);

    // Listener for pressing submit button
    // Streak will only be updated
    if (streakUpdate) {
        btnSubmit.addEventListener('click', () => {
            setItem(`${streakID}Name`, formInput.value);
            location.reload();
        }, {once: true});
    } else {
        // Streak will be entirely new
        btnSubmit.addEventListener('click', () => {
                if (formInput.value !== 'streaks') {
                    if (formInput.value.length > 0) {
                        createStreak(formInput.value)
                    } else {
                        sysMessage('Please enter a streak name')
                    }
                } else {
                    sysMessage('Please choose another name, this one is reserved.')
                }
            }
            , {once: true});
    }

    // Cancel button
    const btnCancel = btnStandard('cancelUpdateStreak', 'Cancel');
    boxShadBoxInner.append(btnCancel);

    window.onclick = function (event) {
        if (event.target.contains(boxStreakFormRow) && event.target !== boxStreakFormRow) {
            undoFormDOM();
        }
    }
    btnCancel.addEventListener('click', () => {
        undoFormDOM();
    }, {once: true});

    return boxStreakFormRow;
}

export default formStreakName